/*!
 * node-ed-mailer
 * AMQP event driven e-mail sender.
 *
 * Copyright(c) 2018 IT Resources s.r.l.
 * Copyright(c) 2018 Luca Stasio <joshuagame@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * See node-ed-mailer license text even in LICENSE file.
 *
 * lib/mailer.js
 */

'use strict';

const config = require('config');
const {application, inject} = require('node-mu').ioc;
const Application = require('node-mu').Application;
const {EventsMediator, AmqpConsumer, SmtpTransporter} = require('./providers');
const {MailSender, MailRenderer} = require('./services');
const path = require('path');

module.exports =
  inject([
      EventsMediator,
      AmqpConsumer,
      SmtpTransporter,
      MailSender,
      MailRenderer
    ],
    application(
      class Mailer extends Application {
        constructor(eventsMediator, amqpConsumer, smtpTrasporter, mailSender, mailRenderer) {
          super();

          // Providers initialization
          this._eventsMediator = eventsMediator;
          this._amqpConsumer = amqpConsumer;
          this._smtpTransporter = smtpTrasporter;

          // services
          this._mailSender = mailSender;
          this._mailRenderer = mailRenderer;

          // mail templates Map initialization
          this._templatesMap = new Map();

          this._logger.info('Mailer service started');
        }

        set templatesConfig(templatesConfig) {
          this._templatesConfig = templatesConfig;
        }

        async $bootstrap() {
          this._logger.info('bootstrap sequence');
          return new Promise((resolve, reject) => {
            try {
              const eventReceived = this._eventsMediator.eventReceived;
              this._eventsMediator.on(eventReceived, (data) => {
                this._onAmqpMessageReceived(data);
              });
              this._logger.info(`"${eventReceived}" internal event handler registered`);

              this._logger.debug(`registering mail templates: ${JSON.stringify(this._templatesConfig)}`);
              this._templatesBasePath = this._templatesConfig.basePath;
              for (let template of this._templatesConfig.templates) {
                this.registerTemplate(template);
              }
              this._logger.info('bootstrap sequence completed');
              resolve();
            } catch (err) {
              reject(err);
            }
          });
        }

        registerTemplate(mailTemplate) {
          this._attachments(mailTemplate.params.attachments);
          this._templatesMap.set(mailTemplate.name, mailTemplate.params);
          this._logger.info(`"${mailTemplate.name}" template registered`);
        }

        /**
         * Event Handler function called when an internal 'event-received' event is emitted (actually from the
         * AmqpConsumer Provider when it consume a message from the AMQP queue).
         *
         * @param evt
         * @returns {Promise<void>}
         * @private
         */
        async _onAmqpMessageReceived(evt) {
          try {
            const evtType = `${evt.type}:${evt.spec}`;
            this._logger.info(`AMQP message received. Evt type: ${evtType}`);
            this._logger.debug(`evt: ${JSON.stringify(evt)}`);

            const templateParams = this._templatesMap.get(evtType);
            if (templateParams) {
              this._logger.debug(`template params: ${JSON.stringify(templateParams)}`);
              const subject = templateParams.subject;
              const htmlFile = this._htmlTemplateFile(templateParams);
              const attachments = templateParams.attachments;
              const to = evt.mailto;

              const html = await this._mailRenderer.render(htmlFile, templateParams.templateData(evt.payload));
              await this._mailSender.send(to, subject, html, attachments);
            }
          } catch (err) {
            throw err;
          }
        }

        _htmlTemplateFile(params) {
          return path.join(this._templatesBasePath, params.htmlTemplate);
        }

        _attachments(attachments) {
          this._logger.debug(`attachments: ${JSON.stringify(attachments, null, 2)}`);
          for (let attachment of attachments) {
            attachment.path = path.join(this._templatesBasePath, attachment.path);
          }
          return attachments;
        }
      }
    )
  );

