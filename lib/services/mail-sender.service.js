/*!
 * node-ed-mailer
 * AMQP event driven e-mail sender.
 *
 * Copyright(c) 2018 IT Resources s.r.l.
 * Copyright(c) 2018 Luca Stasio <joshuagame@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * See node-ed-mailer license text even in LICENSE file.
 *
 * lib/services/mail-sender.service.js
 */

'use strict';

const {inject, service} = require('node-mu').ioc;
const {Service} = require('node-mu');
const {SmtpTransporter} = require('../providers');

module.exports =
  inject(
    [SmtpTransporter],
    service(
      class MailSender extends Service {
        constructor(smtpTransporter) {
          super();
          this._smtpTransporter = smtpTransporter;
        }

        send(to, subject, messageBody, attachments) {
          this._logger.info(`sending email to ${to}`);
          this._logger.debug(`email message body:\n${messageBody}`);
          let message = {
            to: to,
            subject: subject,
            html: messageBody
          };

          if (attachments.length > 0) {
            message.attachments = [];
            for (let attachment of attachments) {
              this._logger.debug(`message attachment: ${attachment.filename}`);
              message.attachments.push(attachment);
            }
          }

          this._smtpTransporter.transporter.sendMail(message, (error, info) => {
            if (error) {
              this._logger.error(`error sending email: ${error.message}`);
            } else {
              this._logger.info('email sent successfully!');
            }
          });
        }

      }
    )
  );