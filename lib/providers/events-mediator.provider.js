/*!
 * node-ed-mailer
 * AMQP event driven e-mail sender.
 *
 * Copyright(c) 2018 IT Resources s.r.l.
 * Copyright(c) 2018 Luca Stasio <joshuagame@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * See node-ed-mailer license text even in LICENSE file.
 *
 * lib/providers/events-mediator.provider.js
 */

'use strict';

const {provider} = require('node-mu').ioc;
const {Provider} = require('node-mu').Providers;
const Mediator = require('hertzy');

module.exports =
  provider(
    class EventsMediator extends Provider {
      constructor() {
        super();

        this._eventChannelName = 'event-channel';
        this._eventReceived = 'event-received';

        this.initEventChannel();
      }

      async $start() {
        return new Promise((resolve, reject) => {
          try {
            this.initEventChannel();
            resolve();
          } catch (err) {
            throw err;
          }
        });
      }

      initEventChannel(eventChannelName = this._eventChannelName) {
        this._channel = Mediator.tune(eventChannelName);
        return this._channel;
      }

      on(evt, evtHandler) {
        this._channel.on(evt, evtHandler);
      }

      get eventChannelName() {
        return this._eventChannelName;
      }

      set eventChannelName(eventChannelName) {
        this._eventChannelName = eventChannelName;
      }

      get eventReceived() {
        return this._eventReceived;
      }

      get channel() {
        return this._channel;
      }
    }
  );