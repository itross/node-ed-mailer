/*!
 * node-ed-mailer
 * AMQP event driven e-mail sender.
 *
 * Copyright(c) 2018 IT Resources s.r.l.
 * Copyright(c) 2018 Luca Stasio <joshuagame@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * See node-ed-mailer license text even in LICENSE file.
 *
 * lib/providers/smtp-transporter.provider.js
 */

'use strict';

const config = require('config');
const {provider} = require('node-mu').ioc;
const {Provider} = require('node-mu').Providers;
const Nodemailer = require('nodemailer');

module.exports =
  provider(
    class SmtpTransporter extends Provider {
      constructor() {
        super();
        this._smtpConf = config.get('smtp');
      }

      async $start() {
        const nodemailerParams = config.get('nodemailer') || { logger: false, debug: false };
        const smtpConfig = config.get('smtp') || {
          host: '127.0.0.1',
          port: 465,
          secure: true,
          tls: {
            rejectUnauthorized: true
          },
          auth: {
            user: 'default',
            pass: 'default'
          }
        };

        const mailDefault = config.get('mailDefault') || { from: 'default@localhost', replyTo: 'noreply@localhost' };

        const transportParams = {
          host: smtpConfig.host,
          port: smtpConfig.port,
          secure: smtpConfig.secure,
          tls: {
            rejectUnauthorized: smtpConfig.reject.unauthorized
          },
          auth: {
            user: smtpConfig.auth.user,
            pass: smtpConfig.auth.password
          },
          logger: nodemailerParams.logger,
          debug: nodemailerParams.debug
        };

        this._transporter = Nodemailer.createTransport(transportParams, mailDefault);
        this._logger.debug('nodemailer SMTP transport initialized');
      }

      get transporter() {
        return this._transporter;
      }
    }
  );