'use strict';

const build = require('node-mu');
const Mailer = require('../../lib');

const start = async () => {
  try {
    const mailerService = build(Mailer);
    mailerService.templatesConfig = require('./templates-config');
    await mailerService.run();
  } catch (err) {
    throw err;
  }
};

start()
  .then(() => {
    console.log(`\uD83D\uDE80  node-\u03BC service started [pid: ${process.pid}]... bring me some \uD83C\uDF7A \uD83C\uDF7A \uD83C\uDF7A`);
  }).catch(err => {
  console.error(`\uD83D\uDD25  service crashed at startup: ${err}`);
  process.exit(1);
});